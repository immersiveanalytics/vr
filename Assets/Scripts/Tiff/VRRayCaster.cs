using UnityEngine;
using System.Collections.Generic;

public class VRRaycaster : MonoBehaviour
{
    public GameObject source;
    public float raycastDistance = 10f; // Distance of the raycast

    private List<GameObject> currentHitObjects = new List<GameObject>();

    void Update()
    {
        Ray ray = new Ray(source.transform.position, source.transform.forward);
        RaycastHit[] hits = Physics.RaycastAll(ray, raycastDistance);

        List<GameObject> hitObjects = new List<GameObject>();
        foreach (RaycastHit hit in hits)
        {
            GameObject hitObject = hit.collider.gameObject;
            hitObjects.Add(hitObject);

            if (!currentHitObjects.Contains(hitObject))
            {
                CubePrefabAttribute cube = hitObject.GetComponent<CubePrefabAttribute>();
                if (cube != null)
                {
                    cube.PlayAudio();
                }
            }
        }

        // Stop audio for objects no longer being hit
       /* foreach (GameObject obj in currentHitObjects)
        {
            if (!hitObjects.Contains(obj))
            {
                CubePrefabAttribute cube = obj.GetComponent<CubePrefabAttribute>();
                if (cube != null)
                {
                    cube.StopAudio();
                }
            }
        }*/

        // Update the list of currently hit objects
        currentHitObjects = hitObjects;
    }
}
