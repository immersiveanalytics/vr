using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

public class CreationSongs : MonoBehaviour
{
    public float baseFrequency = 440.0f;
    public float duration = 1.0f;
    public float frequencyOffset = 0.01f;
    public Matrix3D filteredMatr; // Use the Matrix3D type directly

    private List<AudioSource> audioSources = new List<AudioSource>();
    private int width ;
    private int height ; 
    private int numDirectories;
    
    void Start()
    {
        // To ensure that loading and playing audio clips occurs correctly
        StartCoroutine(LoadAndPlayAudioClips());
    }

    IEnumerator LoadAndPlayAudioClips()
    {
        // Wait for filteredMatr to be initialized
        while (filteredMatr == null)
        {   
            // Find the TiffSlicesLoader object in the scene    
            TiffSlicesLoader tiffLoader = FindObjectOfType<TiffSlicesLoader>();
            if (tiffLoader != null)
            {
                filteredMatr = tiffLoader.GetFilteredMatrix();
            }
            yield return null;
        }

        width = filteredMatr.Width;
        height = filteredMatr.Height;
        numDirectories = filteredMatr.Depth;
        
        Debug.Log("Filtered matrix initialized. Creating audio files... ");
        // Create audio files
        CreateAudioFiles(numDirectories, width, height);

        // Wait until all audio clips are loaded
        while (audioSources.Count < width * height * numDirectories)
        {
            yield return null;
        }

        // Play the audio clips
        foreach (var audioSource in audioSources)
        {
            audioSource.Play();
            Debug.Log("Playing AudioClip: " + audioSource.clip.name);
            yield return new WaitForSeconds(audioSource.clip.length + 0.5f);
        }
    }

    void CreateAudioFiles(int numDirectories, int width, int height)
    {
        for (int i = 0; i < numDirectories; i++)
        {
            string directoryPath = Path.Combine(Application.persistentDataPath, "AudioFiles", "Directory_" + i);
            Debug.Log("Creating directory at: " + directoryPath);
            if (!Directory.Exists(directoryPath))
            {
                Directory.CreateDirectory(directoryPath);
            }

            for (int w = 0; w < width; w++)
            {
                for (int h = 0; h < height; h++)
                {
                    string filePath = Path.Combine(directoryPath, $"Audio_{w}_{h}.wav");
                    float intensity = filteredMatr.GetValue(i, w, h);

                    // Example: Adjusting frequency based on intensity
                    float frequency = baseFrequency + intensity * frequencyOffset;

                    // Create the audio file with the calculated frequency
                    CreateSineWave(filePath, frequency, duration);
                }
            }
        }
    }

    void CreateSineWave(string filePath, float frequency, float duration)
    {
        int sampleRate = 44100;
        int sampleCount = (int)(sampleRate * duration);
        float[] samples = new float[sampleCount];
        for (int i = 0; i < sampleCount; i++)
        {
            samples[i] = Mathf.Sin(2 * Mathf.PI * frequency * i / sampleRate);
        }

        try
        {
            Debug.Log("Creating WAV file at: " + filePath);
            using (var fileStream = new FileStream(filePath, FileMode.Create))
            {
                WriteWavFile(fileStream, samples, sampleRate);
            }
        }
        catch (Exception e)
        {
            Debug.LogError("Error creating WAV file at " + filePath + ": " + e.Message);
            return;
        }

        StartCoroutine(LoadClipAndAddSource(filePath));
    }

    void WriteWavFile(FileStream fileStream, float[] samples, int sampleRate)
    {
        int sampleCount = samples.Length;
        int byteRate = sampleRate * 2;
        int blockAlign = 2;

        using (BinaryWriter writer = new BinaryWriter(fileStream))
        {
            writer.Write("RIFF".ToCharArray());
            writer.Write(36 + sampleCount * 2);
            writer.Write("WAVE".ToCharArray());

            writer.Write("fmt ".ToCharArray());
            writer.Write(16);
            writer.Write((short)1);
            writer.Write((short)1);
            writer.Write(sampleRate);
            writer.Write(byteRate);
            writer.Write((short)blockAlign);
            writer.Write((short)16);

            writer.Write("data".ToCharArray());
            writer.Write(sampleCount * 2);
            foreach (var sample in samples)
            {
                writer.Write((short)(sample * short.MaxValue));
            }
        }
    }

    IEnumerator LoadClipAndAddSource(string filePath)
    {
        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip("file://" + filePath, AudioType.WAV))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError("Failed to load AudioClip from " + filePath + ": " + www.error);
                yield break;
            }

            AudioClip audioClip = DownloadHandlerAudioClip.GetContent(www);

            GameObject audioObject = new GameObject(Path.GetFileNameWithoutExtension(filePath));
            AudioSource audioSource = audioObject.AddComponent<AudioSource>();
            audioSource.clip = audioClip;
            audioSource.volume = 1.0f; // Ensure volume is correctly set
            audioSource.playOnAwake = false; // Prevent automatic playback
            audioSources.Add(audioSource);

            Debug.Log("AudioClip loaded: " + filePath);
        }
    }
}
