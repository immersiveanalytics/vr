using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class CubePrefabAttribute : MonoBehaviour
{
    private AudioSource audioSource;

    private float intensity;

    void Start()
    {
        // Get the AudioSource component attached to the CubePrefabObject
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        // You can put any general update logic here if needed
    }

    public void SetIntensity(float newIntensity)
    {
        intensity = newIntensity;

        // Get the corresponding audio clip from the AudioManager and play 
        LoadNewAudioClip();
    }

    private void LoadNewAudioClip()
    {
        AudioClip clip = AudioManager.Instance.GetAudioClip(intensity);

        if (audioSource == null)
        {
            Debug.LogError("audio source is null");
            audioSource = GetComponent<AudioSource>();
        }

        // Check if the clip is different from the currently playing clip
        if (audioSource.clip != clip)
        {
            // Set the audio clip
            audioSource.clip = clip;
        }
    }

    public void PlayAudio()
    {
        if (audioSource != null && audioSource.clip != null && !audioSource.isPlaying)
        {
            // ensure that the audio is not playing
            audioSource.Play();
        }
    }

    public void StopAudio()
    {
        if (audioSource != null && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }
}