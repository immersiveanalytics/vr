using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorIntensity
{
    public static float GetIntensity(Color32 color)
    {
        float intensity = 0.2126f * color.r + 0.7152f * color.g + 0.0722f * color.b;
        return intensity / 255f; // Normalize to a range of 0 to 1
    }
}
