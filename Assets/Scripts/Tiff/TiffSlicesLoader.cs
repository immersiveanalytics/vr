using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BitMiracle.LibTiff.Classic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;
using System;

public class TiffSlicesLoader : MonoBehaviour
{
    public string tiffFilePath;
    public GameObject slicePrefab; // Prefab with a renderer, used as building blocks
    private Matrix3D intensityMatr = null; // The global matrix which contains all pixels from the original TIFF image
    private Matrix3D filteredMatr = null; // Matrix with a reduced set of points, the reduction is made according to the function redux in Matrix3D
    private float maxIntensity = 0;

    private Hashtable fabs = new Hashtable();
    private int currentDir = 0;
    private bool play = true;

    /* 
     * 
     * Customization parameters 
     * 
     */

    // Position of prefab object on the plan
    private float startX = 0.0f;
    private float startZ = 0.0f;
    private float startY = 0.2f;

    // Portion of the image to consider
    public int startWidth = 100;
    public int endWidth = 102;
    public int startHeight = 100;
    public int endHeight = 102;

    // Pixel bining on the portion to consider
    public int reduxCoef = 4;

    // Vetical scale of the prefab objects
    public float YMax = 1.0f;
    public float spacing = 0.4f;

    // Speed
    public float updateInterval = 1.0f; // Intervalle en secondes
    private float timeSinceLastUpdate = 0.0f;


    // Input Action Reference
    public InputActionReference playPauseAction;

    void Start()
    {
        LoadTiffSlices(tiffFilePath);
        prepareFabs(filteredMatr);

        // Bind the action to the method
        playPauseAction.action.performed += OnPlayPause;
    }

    void Update()
    {
        timeSinceLastUpdate += Time.deltaTime;

        if (play && timeSinceLastUpdate > updateInterval)
        {
            timeSinceLastUpdate = 0.0f;
            currentDir = (currentDir + 1) % 92; // cheating
            SpawnElements(filteredMatr, currentDir);
            UpdateColors(filteredMatr, currentDir);
            // playAllAudio();
        }
    }

    // LoadTiffSlices turns a tiff image into one global Matrix3D of intensity and a Filtered Matrix3D containing the part of each image chosen with the customization params
    void LoadTiffSlices(string filePath)
    {
        Tiff image = Tiff.Open(filePath, "r");
        if (image == null)
        {
            Debug.LogError("Could not open TIFF file.");
            return;
        }

        int width = image.GetField(TiffTag.IMAGEWIDTH)[0].ToInt();
        int height = image.GetField(TiffTag.IMAGELENGTH)[0].ToInt();
        int numberOfSlices = image.NumberOfDirectories();

        int fWidth = (endWidth - startWidth) / reduxCoef;
        int fHeight = (endHeight - startHeight) / reduxCoef;

        // if the rest is not null we add one column and one row for safety 
        if ((endWidth - startWidth) % reduxCoef != 0)
        {
            fWidth += 1;
        }

        if ((endHeight - startHeight) % reduxCoef != 0)
        {
            fHeight += 1;
        }

        // Matrix Declaration
        intensityMatr = new Matrix3D(numberOfSlices, width, height);
        filteredMatr = new Matrix3D(numberOfSlices, fWidth, fHeight);

        for (int dir = 0; dir < numberOfSlices; dir++)
        {
            image.SetDirectory((short)dir);
            int[] raster = new int[width * height];

            if (!image.ReadRGBAImage(width, height, raster))
            {
                Debug.LogError("Could not read TIFF image.");
                return;
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    int pixel = raster[i * height + j];
                    Color32 color = new Color32(
                        (byte)Tiff.GetR(pixel),
                        (byte)Tiff.GetG(pixel),
                        (byte)Tiff.GetB(pixel),
                        (byte)Tiff.GetA(pixel)
                    );
                    float intensity = ColorIntensity.GetIntensity(color);
                    intensityMatr.SetValue(dir, i, j, intensity);

                    if (intensity > maxIntensity)
                    {
                        maxIntensity = intensity;
                    }
                }
            }
        }


        filteredMatr.FilterMatrix(intensityMatr, startWidth, endWidth, startHeight, endHeight, reduxCoef);

        // Global informations on the loading
        Debug.Log("Number of slices: " + numberOfSlices);
        Debug.Log("Max Intensity : " + maxIntensity + " out of 1");
        Debug.Log("Original Image dimension : Width = " + width + " | Height = " + height);
        Debug.Log("Filtered matrix dimensions: " + filteredMatr.Width + " x " + filteredMatr.Height);
        image.Close();
    }

    private void prepareFabs(Matrix3D mat)
    {
        // to be called only when the filteredMat is correctly initiated.
        startX = (-1.0f) * mat.Width / 2;
        startZ = (-1.0f) * mat.Height / 2;

        Debug.Log("First Creation of cubes elements");
        for (int i = 0; i < mat.Width; i++)
        {
            for (int j = 0; j < mat.Height; j++)
            {
                // spawn elements of the filtered matrix here.
                GameObject pixel3D = Instantiate(slicePrefab, new Vector3(startX + i * spacing,
                                                                            startY + mat.GetValue(currentDir, i, j) * YMax,
                                                                            startZ + j * spacing
                                                                         ), Quaternion.identity);
                pixel3D.name = "Voxel" + i + ":" + j;

                //  set the value of the inensity of this pixel inside the Given variable.
                CubePrefabAttribute cubeAttr = pixel3D.GetComponent<CubePrefabAttribute>();
                if (cubeAttr != null)
                {
                    cubeAttr.SetIntensity(mat.GetValue(currentDir, i, j));
                }
                else
                {
                    Debug.Log("Cannot set intensity for pixel " + i + " : " + j);
                }

                fabs.Add(new Vector2(i, j), pixel3D);
            }
        }
    }

    private void SpawnElements(Matrix3D mat, int dir)
    {
        Vector3 pos;

        foreach (DictionaryEntry entry in fabs)
        {
            Vector2 key = (Vector2)entry.Key;
            GameObject p = (GameObject)entry.Value;

            // update the position on all axis consedering that the spacing can change  
            float intensity = mat.GetValue(dir, (int)key.x, (int)key.y);
            
            pos = p.transform.position;
            pos.x = startX + key.x * spacing;
            pos.y = startY + intensity * YMax;
            pos.z = startZ + key.y * spacing;


            p.transform.position = pos;

            // update the value of intensity for the cubeAttribute
            p.GetComponent<CubePrefabAttribute>().SetIntensity(intensity);
        }
    }

    private void UpdateColors(Matrix3D mat, int dir)
    {
        // Normalize the currentDir to a value between 0 and 1
        float normalizedDir = (float)dir / 91.0f;
        Color color = Color.Lerp(Color.blue, Color.red, normalizedDir);

        // Debug.Log($"Run for {currentDir} with color {color}");

        foreach (DictionaryEntry entry in fabs)
        {
            Vector2 key = (Vector2)entry.Key;
            GameObject p = (GameObject)entry.Value;

            // Map normalizedDir to color (blue to red)

            Renderer renderer = p.GetComponent<Renderer>();
            if (renderer != null && renderer.material != null)
            {
                renderer.material.SetColor("_TintColor", color);
            }
        }
    }


    public Matrix3D GetFilteredMatrix()
    {
        return filteredMatr;
    }

    public void updateMaxHeight(float newMaxHeight)
    {
        YMax = newMaxHeight;
        // Debug.Log("New Height set to " + newMaxHeight);
    }

    public void updateSpacing(float newSpacing)
    {
        spacing = newSpacing;
        // Debug.Log("New Spacing set to " + spacing);
    }

    public void updateSpeed(float newSpeed)
    {
        updateInterval = newSpeed;
    }

    public void togglePlay()
    {
        play = !play;
    }

    public void playAllAudio()
    {
        Debug.Log("Play all audio button pressed");
        foreach (DictionaryEntry entry in fabs)
        {
            GameObject p = (GameObject)entry.Value;
            p.GetComponent<CubePrefabAttribute>().PlayAudio();
        }
    }

    void OnPlayPause(InputAction.CallbackContext context)
    {
        togglePlay();
        Debug.Log("Play button hitted");
    }

    public int GetCurrentDir()
    {
        return currentDir;
    }
    // Don't forget to create a mapping for this.

    private void OnDestroy()
    {
        // Unbind the action when the object is destroyed
        playPauseAction.action.performed -= OnPlayPause;
    }
}
