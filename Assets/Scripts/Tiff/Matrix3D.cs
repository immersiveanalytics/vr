using UnityEngine;

// Wrapper of a normal matrix with method for reduction, filter and extraction.
public class Matrix3D
{
    private float[][][] matrix;
    public int Depth { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    public Matrix3D(int depth, int width, int height)
    {
        Depth = depth;
        Width = width;
        Height = height;
        matrix = new float[depth][][];
        for (int d = 0; d < depth; d++)
        {
            matrix[d] = new float[width][];
            for (int w = 0; w < width; w++)
            {
                matrix[d][w] = new float[height];
            }
        }
    }

    public void SetValue(int depth, int width, int height, float value)
    {
        matrix[depth][width][height] = value;
    }

    public float GetValue(int depth, int width, int height)
    {
        return matrix[depth][width][height];
    }

    public void FilterMatrix(Matrix3D sourceMatrix, int startWidth, int endWidth, int startHeight, int endHeight, int reduxCoef)
    {
        for (int dir = 0; dir < Depth; dir++)
        {
            for (int i = startWidth; i < endWidth; i += reduxCoef)
            {
                int filteredIndexI = (i - startWidth) / reduxCoef;
                for (int j = startHeight; j < endHeight; j += reduxCoef)
                {
                    int filteredIndexJ = (j - startHeight) / reduxCoef;
                    float[][] subMatrix = ExtractSubMatrix(sourceMatrix, dir, i, j, reduxCoef);
                    SetValue(dir, filteredIndexI, filteredIndexJ, Redux(subMatrix));
                }
            }
        }
    }

    private float[][] ExtractSubMatrix(Matrix3D sourceMatrix, int depth, int startI, int startJ, int reduxCoef)
    {
        int subMatrixWidth = reduxCoef;
        int subMatrixHeight = reduxCoef;
        float[][] subMatrix = new float[subMatrixWidth][];
        for (int i = 0; i < subMatrixWidth; i++)
        {
            subMatrix[i] = new float[subMatrixHeight];
            int maxI = startI + i > sourceMatrix.Width ? sourceMatrix.Width : startI + i;
            for (int j = 0; j < subMatrixHeight; j++)
            {
                int maxJ = startJ + j > sourceMatrix.Height ? sourceMatrix.Height : startJ + j;
                subMatrix[i][j] = sourceMatrix.GetValue(depth, maxI, maxJ);
            }
        }
        return subMatrix;
    }

    private float Redux(float[][] subMatrix)
    {
        // Example reduction logic: calculate the average intensity of the subMatrix
        float sum = 0;
        int count = 0;

        for (int i = 0; i < subMatrix.Length; i++)
        {
            for (int j = 0; j < subMatrix[i].Length; j++)
            {
                sum += subMatrix[i][j];
                count++;
            }
        }

        return sum / count;
    }
}
