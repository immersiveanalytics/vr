using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance { get; private set; }

    public AudioClip[] pianoNotes;
    public AnimationCurve intensityToNoteCurve;

    private void Awake()
    {
        // Singleton pattern to ensure only one instance of AudioManager exists
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public AudioClip GetAudioClip(float intensity)
    {
        if (pianoNotes == null || pianoNotes.Length == 0)
        {
            Debug.LogError("AudioManager: pianoNotes array is not set up.");
            return null;
        }

        // Evaluate the curve to get the note index
        float curveValue = intensityToNoteCurve.Evaluate(intensity);
        int noteIndex = Mathf.FloorToInt(curveValue * (pianoNotes.Length - 1));

        return pianoNotes[noteIndex];
    }
}
