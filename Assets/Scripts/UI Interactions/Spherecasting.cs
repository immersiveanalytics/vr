using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spherecasting : MonoBehaviour
{
    public int _sphereRadius = 1;
    
    private Camera _mainCamera;
    private const int _spherecastDistance = 10; // ray lenght

    private LayerMask layerGrabbable;

    // Dictionary to store the original colors of hit objects
    private Dictionary<Renderer, Color> originalColors = new Dictionary<Renderer, Color>();
    // List to store the currently hit objects
    private List<Renderer> currentlyHitRenderers = new List<Renderer>();

    
    void Start()
    {
        // Search the component camera in the scene
        _mainCamera = Camera.main;

        if (_mainCamera == null)
        {
            Debug.LogError("Main camera not found. Please ensure there is a camera with the tag 'MainCamera'.");
        }

        layerGrabbable = LayerMask.GetMask("Grabbable");

        // The mouse cursor is forced to remain in the Game window
        Cursor.lockState = CursorLockMode.Confined;
    }

    void FixedUpdate()
    {
        if (_mainCamera == null)
            return;

        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        List<RaycastHit> hits = new List<RaycastHit>(Physics.SphereCastAll(ray, _sphereRadius, _spherecastDistance, layerGrabbable));

        // Reset colors of previously hit objects if they are not hit in this frame
        foreach (Renderer renderer in currentlyHitRenderers)
        {
            if (renderer != null && !hits.Exists(hit => hit.transform.GetComponent<Renderer>() == renderer))
            {
                renderer.material.color = originalColors[renderer];
            }
        }

        // Clear the list of currently hit objects for this frame
        currentlyHitRenderers.Clear();

        if (hits.Count > 0)
        {
            foreach (RaycastHit hit in hits)
            {
                Renderer rend = hit.transform.GetComponent<Renderer>();
                if (rend != null)
                {
                    // Store the original color if not already stored
                    if (!originalColors.ContainsKey(rend))
                    {
                        originalColors[rend] = rend.material.color;
                    }

                    // Change the color of the hit object
                    rend.material.color = Color.red;

                    // Add to the list of currently hit objects
                    currentlyHitRenderers.Add(rend);
                }
            }
        }
    }

    void OnDrawGizmos()
    {
        if (_mainCamera == null)
            return;

        // Display the explosion radius when selected
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(ray.GetPoint(_spherecastDistance), _sphereRadius);
    }
}
