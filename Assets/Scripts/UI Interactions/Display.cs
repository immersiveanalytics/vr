using UnityEngine;
using TMPro;

public class Display : MonoBehaviour
{
    public Camera vrCamera; // Reference to the VR Camera
    public string tiffSlicesLoaderObjectName = "Manager"; // Name of the GameObject with the TiffSlicesLoader script

    public TMP_Text currentDirText;
    private TiffSlicesLoader tiffSlicesLoader; // Reference to the TiffSlicesLoader script

    void Start()
    {
        // Find the TiffSlicesLoader GameObject by name and get its TiffSlicesLoader component
        GameObject tiffSlicesLoaderObject = GameObject.Find(tiffSlicesLoaderObjectName);
        if (tiffSlicesLoaderObject != null)
        {
            tiffSlicesLoader = tiffSlicesLoaderObject.GetComponent<TiffSlicesLoader>();
        }
        else
        {
            Debug.LogError("Could not find the TiffSlicesLoaderObject. Please ensure it is named correctly.");
        }

        if (tiffSlicesLoader == null)
        {
            Debug.LogError("TiffSlicesLoader reference is missing. Please assign it in the inspector.");
        }

        // Ensure currentDirText is initialized correctly
        if (currentDirText == null)
        {
            Debug.LogError("CurrentDirText reference is missing. Please assign it in the inspector.");
        }
    }

    void Update()
    {
        if (tiffSlicesLoader != null && currentDirText != null)
        {
            // Update the Text component with the currentDir value
            currentDirText.text = $"{tiffSlicesLoader.GetCurrentDir()} / 91";
        }

        // Make the Canvas face the VR camera
        transform.rotation = Quaternion.LookRotation(transform.position - vrCamera.transform.position);

        // Calculate the position to the top-left corner of the camera's view
        float distanceFromCamera = 2.0f; // Adjust distance as needed
        Vector3 topLeftScreen = new Vector3(0, vrCamera.pixelHeight, vrCamera.nearClipPlane);

        // Convert screen point to world point
        Vector3 worldPosition = vrCamera.ScreenToWorldPoint(topLeftScreen);

        // Calculate offset from the top-left corner
        Vector3 offset = vrCamera.transform.right * 0.1f + vrCamera.transform.up * -0.1f; // Adjust offset values as needed

        // Set the position of the canvas
        transform.position = vrCamera.transform.position + vrCamera.transform.forward * distanceFromCamera + offset;

        // Optionally, you can set the scale of the Canvas if needed
        // transform.localScale = new Vector3(0.001f, 0.001f, 0.001f); // Adjust the scale as needed
    }


}
