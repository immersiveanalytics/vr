using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.XR;

public class Menu : MonoBehaviour
{
    // Input Action Reference
    public InputActionReference menuAction;

    // The menu GameObject
    public GameObject menu;

    // The XR Origin camera
    public Transform cameraTransform;

    // Toggle state for the menu
    private bool isMenuVisible = false;

    void Start()
    {
        // Bind the action to the method
        menuAction.action.performed += OnMenu;

        // Initially hide the menu
        menu.SetActive(false);
    }

    void OnMenu(InputAction.CallbackContext context)
    {
        // Toggle the menu visibility
        isMenuVisible = !isMenuVisible;
        Debug.Log("Menu pushed");
        if (isMenuVisible)
        {
            // Position the menu in front of the camera
            PositionMenuInFrontOfCamera();
            menu.SetActive(true);
        }
        else
        {
            menu.SetActive(false);
        }
    }

    private void PositionMenuInFrontOfCamera()
    {
        // Position the menu a fixed distance in front of the camera
        float distanceFromCamera = 2.0f; // Adjust the distance as needed
        menu.transform.position = cameraTransform.position + cameraTransform.forward * distanceFromCamera;

        // Optionally, you can also align the menu to face the camera
        menu.transform.LookAt(cameraTransform);
        menu.transform.rotation = Quaternion.Euler(0, menu.transform.rotation.eulerAngles.y, 0);
    }

    private void OnDestroy()
    {
        // Unbind the action when the object is destroyed
        menuAction.action.performed -= OnMenu;
    }
}
