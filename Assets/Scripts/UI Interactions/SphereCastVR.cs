using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCastVR : MonoBehaviour
{
    // SphereCast parameters
    public float sphereRadius = 0.5f; // Radius of the sphere at the end of the ray
    public float maxDistance = 10f;   // Maximum distance of the SphereCast
    public LayerMask targetLayer;     // Layer to interact with

    // Color settings
    public Color highlightColor = Color.red;
    private Dictionary<Renderer, List<Color>> originalColors = new Dictionary<Renderer, List<Color>>();
    private GameObject lastHitObject;

    // Reference to the VR controller (e.g., right hand controller)
    public Transform vrControllerTransform;

    // Debug parameters
    private Vector3 sphereCenter;
    private bool hitDetected = false;

    void Update()
    {
        PerformSphereCast();
    }

    void PerformSphereCast()
    {
        // Cast a sphere along the forward direction of the VR controller
        RaycastHit hit;
        Vector3 origin = vrControllerTransform.position;
        Vector3 direction = vrControllerTransform.forward;

        // Perform the SphereCast
        if (Physics.SphereCast(origin, sphereRadius, direction, out hit, maxDistance, targetLayer))
        {
            Debug.Log("Hit: " + hit.collider.name);
            hitDetected = true;
            sphereCenter = hit.point;

            // Highlight the hit object
            GameObject hitObject = hit.collider.gameObject;
            HighlightObject(hitObject);
        }
        else
        {
            hitDetected = false;

            // Remove highlight if no hit
            RemoveHighlight();
        }
    }

    void HighlightObject(GameObject hitObject)
    {
        if (lastHitObject != hitObject)
        {
            // Remove highlight from the last hit object
            if (lastHitObject != null)
            {
                RemoveHighlight();
            }

            // Store the original colors and apply the highlight color
            Renderer renderer = hitObject.GetComponent<Renderer>();
            if (renderer != null)
            {
                List<Color> colors = new List<Color>();
                foreach (var material in renderer.materials)
                {
                    colors.Add(material.color);
                    material.color = highlightColor;
                }
                originalColors[renderer] = colors;
                lastHitObject = hitObject;
            }
        }
    }

    void RemoveHighlight()
    {
        if (lastHitObject != null)
        {
            Renderer renderer = lastHitObject.GetComponent<Renderer>();
            if (renderer != null && originalColors.ContainsKey(renderer))
            {
                // Restore the original colors
                List<Color> colors = originalColors[renderer];
                for (int i = 0; i < renderer.materials.Length; i++)
                {
                    renderer.materials[i].color = colors[i];
                }
                originalColors.Remove(renderer);
            }
            lastHitObject = null;
        }
    }

    void OnDrawGizmos()
    {
        if (vrControllerTransform == null) return;

        // Draw a line for the ray
        Gizmos.color = Color.blue;
        Vector3 direction = vrControllerTransform.forward * maxDistance;
        Gizmos.DrawRay(vrControllerTransform.position, direction);

        // Draw a sphere at the end of the ray
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(vrControllerTransform.position + direction, sphereRadius);

        // Draw a sphere at the hit point if hit detected
        if (hitDetected)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(sphereCenter, sphereRadius);
        }
    }
}
